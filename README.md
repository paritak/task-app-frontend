# task-app-frontend

This application to demonstrate simple ajax api call.
It display dashboard summary details in graph and list of adults records with basic filter and pagination.
I have implemented ```caching``` for api response and used ```SCSS``` for CSS writing.

## Application frameworks

- Angular JS 1.6.6
- CSS : Bootstrap 3.3.7
- Charts : D3.js and C3.js
- Grunt: The JavaScript Task Runner
- Load files Dynamically : oclazyload 1.0.10



## Web Server

- Nginx

## Installation

* nginx - https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/

------------------OR------------------

```
    $ sudo apt-get install nginx
```

## Configuration

#### nginx application configuration - https://www.nginx.com/resources/wiki/start/topics/examples/full/

#### nginx configure reverse proxy - https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/

------------------OR------------------

#### Insert the following block in etc/nginx/nginx.conf file inside http block and change directory path and API pointing.

```
server {
        ##listen 80 default_server;
        listen         8098;
        server_name localhost;

        root /home/task-app-frontend/app; ## path of frontend application root directory

        index index.html index.htm;

        location / {
                default_type  application/octet-stream;
                include  /etc/nginx/mime.types;
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.

                try_files $uri $uri/ =404;
        }
        location ^~ /api/ {
                proxy_pass http://127.0.0.1:5050/; ## IP address and port on which API is running
        }
}

```

#### After saving changes run following command

```sh
    $ sudo service nginx restart
```


## Run Appication

You can check application is running at following url in browser.
Port can be specified in the nginx configuration block, if not specified then default will be 80.

* http://localhost:8098


# Create production level Build folder.

I have implemented grunt for optimization and performance improvement of Application. Task implemented for Grunt are :

- Sass processing,
- concatenation and minification of assets (js and css),
- watching for sass changes,
- cleaning and copying files and folders,
- versioning of asset files


## Installation of Grunt and its dependencies (node.js)

- Grunt install - https://gruntjs.com/installing-grunt

------------------OR------------------

```sh
    $ sudo apt-get install nodejs-legacy
    $ sudo apt-get install npm
    $ sudo npm install -g grunt
    $ sudo npm install -g grunt-cli
```


#### After the successful installation of grunt run the following commands to install project specific module dependencies.

```sh
    $ cd to project root directory
    $ npm install
```


## To generate build version of project

```sh
    $ cd to project root directory
    $ grunt build
```

#### After successful completion of ```grunt build```, in project directory ```app``` folder will be created. You can give path of app folder in nginx configuration to check functionality.

