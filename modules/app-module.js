/**
 * Created by liveongo on 15/6/17.
 */

var demoApp = angular.module('demoApp',['ui.router', 'oc.lazyLoad', 'ngStorage', 'ui.bootstrap']);

demoApp.config(function($urlRouterProvider, $stateProvider, $locationProvider) {

    $urlRouterProvider.otherwise('/dashboard');

    $locationProvider.hashPrefix(''); //to remove ! mark after #

    $stateProvider
        .state('root',{
            url:'',
            abstract:true,
            views: {
                'main': {
                    templateUrl: 'modules/home/views/root.html',
                    controller:'homeController',
                    controllerAs:'home'
                }
            },
            resolve:{
                myController:['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load("modules/home/controllers/home-controller.js");
                }]
            }
        })
        .state('root.dashboard',{
            url:'/dashboard',
            views: {
                'content': {
                    templateUrl: 'modules/home/views/dashboard.html'
                }
            }
        })
        .state('root.list',{
            url:'/list',
            views: {
                'content': {
                    templateUrl: 'modules/home/views/list.html'
                }
            }
        });
    });

