'use strict';

angular.module('demoApp')
    .controller('homeController',function($rootScope, $scope, $http, $state, $window, $localStorage, commonService, $stateParams) {
        var self = this;

        self.selectedMenu = 'dashboard';
        self.selectedFilter = {};

        self.filterObj = {};
        self.filterColumns = ['relationship', 'race', 'sex'];

        self.distinctValues = {
            "relationship":["Not-in-family", "Own-child", "Other-relative", "Husband", "Wife", "Unmarried"],
            "race": ["Black", "Asian-Pac-Islander", "White", "Other", "Amer-Indian-Eskimo"],
            "sex": ["Female", "Male"]
        };

        if($state.current.name != undefined){
            self.selectedMenu = $state.current.name.split('.')[1];
        }
        self.columnList = ['age', 'workclass', 'fnlwgt', 'education', 'education_num', 'marital_status', 'occupation', 'relationship', 'race', 'sex', 'capital_gain', 'capital_loss', 'hours_per_week', 'native_country', 'predicted_value'];

        self.totalCount = 0;
        self.currentPage = 1;

        self.pageLimit = 10;
        self.pageOffset = 0;

        self.itemsPerPageChange = function (itemCount) {
            if(self.pageLimit != itemCount){
                self.pageLimit = itemCount;
                self.currentPage = 1;
                self.getList();
            }
        };

        self.applyFilter = function () {
            console.log(self.selectedFilter);
            for(var column in self.selectedFilter){
                for(var value in self.selectedFilter[column]){
                    if(self.selectedFilter[column][value] == true){
                        if( !(column in self.filterObj) ){
                            self.filterObj[column] = [];
                        }
                        if(self.filterObj[column].indexOf(value) == -1){
                            self.filterObj[column].push(value);
                        }
                    }
                    else{
                        if(column in self.filterObj) {
                            var index = self.filterObj[column].indexOf(value);
                            if (index > -1) {
                                self.filterObj[column].splice(index, 1);
                            }
                        }
                    }
                }
            }
            self.currentPage = 1;
            self.getList();
        };

        self.clearFilter = function (){
            self.selectedFilter = {};
            self.filterObj = {};
            self.currentPage = 1;
            self.getList();
        };

        self.clearFilterItem = function (column, value) {
            self.selectedFilter[column][value] = false;
            var index = self.filterObj[column].indexOf(value);
            self.filterObj[column].splice(index, 1);
            if(self.filterObj[column].length == 0){
                delete self.filterObj[column];
            }
            self.getList();
        };

        self.summaryDetails = function () {
            var options = {
                addDomain: true,
                caching: true
            };
            commonService.loader(true);
            commonService.ajaxCall('GET', "adults/summary", options)
                .then(function (data) {
                    console.log(data);
                    var colors = {'Male': '#1A936F', 'Female': '#125469'};
                    commonService.drawPieChart("gender_distribution", data.data.gender_distribution, colors);
                    commonService.drawBarChart("relationship_distribution", data.data.relationship_distribution, "relationship", "value", '#1A936F', 40);
                    commonService.loader();
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.status_code == 400) {
                        commonService.showError(error.msg.error);
                    }
                    else {
                        commonService.showError("Something went wrong");
                    }
                    commonService.loader();
                })
        };

        self.getList = function () {

            var options = {
                addDomain: true,
                caching:true,
                data:{
                    "filter":self.filterObj
                }
            };

            self.pageOffset = (self.currentPage - 1) * self.pageLimit;

            self.adultList = [];
            commonService.loader(true, "Fetching records");
            commonService.ajaxCall('POST', "adults/list?limit="+self.pageLimit+"&offset="+self.pageOffset, options)
                .then(function (data) {
                    console.log(data);
                    self.adultList = data.data.list;
                    self.totalCount = data.data.count;
                    commonService.loader();
                })
                .catch(function (error) {
                    console.log(error);
                    if(error.status_code == 400){
                        commonService.showError(error.msg.error);
                    }
                    else {
                        commonService.showError("Something went wrong");
                    }
                    commonService.loader();
                })
        }

    });
