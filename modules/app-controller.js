'use strict';

/**
 * Main module of the application.
 */

angular.module('demoApp')
    .controller('appController',function($rootScope, $scope, $http, $state, $window, $localStorage, commonService) {
        var self=this;

        $rootScope.currentState = $state;
        $rootScope.commonService = commonService;
        $rootScope.Math = window.Math;
        $rootScope.parseInt = parseInt;
        $rootScope.Object = Object;

        // $rootScope.length = function (dict_obj) {
        //     return Object.values(JSON.parse(angular.toJson(dict_obj))).length;
        // };

        $rootScope.length = function (dict_obj) {
            if(dict_obj != undefined) {
                return Object.keys(dict_obj).length;
            }
            return 0;
        };




    });
