module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        useminPrepare: {
            html: [ 'index.html'],
            options: {
                dest: 'app'
            }
        },
        usemin: {
            html: ['app/index.html']
        },
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            src: {
                files: grunt.file.expandMapping(['modules/**/*.js', 'common_scripts/**/*.js'], '.tmp/min-safe/', {
                    rename: function(destBase, destPath) {
                        // return destBase+destPath.replace('.css', '.min.css');
                        return destBase+destPath
                    }
                })
            }
        },
        versioning: {
            modules: {
                options: {
                    grepFiles: [
                        'app/scripts/app.min.js'
                    ],
                    keepOriginal: false
                },
                src: ['app/modules/**/*.js']
            },
            app: {
                options: {
                    grepFiles: [
                        'app/index.html'
                    ],
                    keepOriginal: false
                },
                src: ['app/scripts/app.min.js', 'app/libs/js/vendor.min.js', 'app/styles/custom.min.css']
            }
        },
        uglify: {
            options: {
                compress: {
                    drop_console: true
                }
            },
            src: {
                files: [{
                    expand: true,
                    src: ['.tmp/min-safe/modules/**/*.js'],
                    dest: 'app/',
                    cwd: '.',
                    rename: function (dst, src) {
                        return dst+src.replace('.tmp/min-safe/','');
                    }
                }]
            }
        },
        copy: {
            app:{
                files: grunt.file.expandMapping(['modules/**/views/*.html', 'images/**/*', 'index.html',  'libs/css/fonts/*'], 'app/', {
                    rename: function(destBase, destPath) {
                        return destBase+destPath
                    }
                })
            }
        },
        sass: {
            app: {
                files: [{
                    expand: true,
                    cwd: 'styles/scss/',
                    src: ['*.scss'],
                    dest: 'styles/css/',
                    ext: '.css'
                }]
            }
        },
        watch: {
            scripts: {
                files: ['**/*.scss'],
                tasks: ['sass'],
                options: {
                    spawn: false,
                },
            },
        },
        clean:{
            tmp:['.tmp'],
            app:['app']
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-version-assets');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('build', 'Start build', function() {
        grunt.task.run('clean:app', 'sass', 'ngAnnotate', 'useminPrepare', 'concat:generated', 'uglify:generated', 'cssmin:generated',
            'uglify', 'copy',  'usemin', 'versioning', 'clean:tmp');
    });

};