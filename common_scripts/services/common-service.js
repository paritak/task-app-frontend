demoApp.service('commonService', function ($q, $rootScope, $http, $ocLazyLoad, $localStorage, $timeout, $compile, $filter, $cacheFactory) {
    // runtime dependencies for the service can be injected here, at the provider.$get() function.

    var self = this;

    self.cache = $cacheFactory('dataCache');

    this.ajaxCall = function (method, url, options) {
        var headers = {};
        var data, caching, domain = "";
        if (options != undefined) {
            if (options.headers != undefined) {
                headers = options.headers;
            }
            if (options.data != undefined) {
                data = options.data;
            }
            if (options.caching != undefined) {
                caching = options.caching;
            }
            if (options.addDomain != undefined) {
                if (options.addDomain == true) {
                    if (options.domain != undefined) {
                        domain = options.domain;
                    }
                    else {
                        domain = 'api/';
                    }
                }
            }
        }
        var enableCache = (caching != undefined && caching != null) ? caching : false;

        var deferred = $q.defer();

        var cachedData;
        if(enableCache == true){
            var cacheId = url + (data != undefined ? angular.toJson(data) : '');
            console.log(data+"="+cacheId);
            cachedData = self.cache.get(cacheId);
        }

        if(cachedData){
            deferred.resolve(cachedData);
        }
        else {


            $http({
                url: domain + url,
                method: method,
                data: data,
                headers: headers,
                cache: enableCache,
                uploadEventHandlers: {
                    progress: function (e) {
                        if (e.lengthComputable) {
                            // $timeout(function () {
                            // deferred.notify((e.loaded/ e.total)*100);
                            deferred.notify({
                                loaded: (e.loaded / 1048576).toFixed(2),
                                total: (e.total / 1048576).toFixed(2),
                                percentage: (e.loaded / e.total) * 100
                            });
                            // },0)
                        }
                    }
                }
            }).then(
                function (data) {
                    if(enableCache == true) {
                        self.cache.put(cacheId, data.data);
                    }
                    deferred.resolve(data.data);
                },
                function (msg, code) {
                    deferred.reject({msg: msg.data, status_code: msg.status});
                    // console.log(msg);
                    if (msg.status == 502) {
                        self.showError("Api server is unreachable.");
                    }
                    //$log.error(msg, code);
                });
        }

        return deferred.promise;
    };


    this.loader = function (flag, loading_msg) {
            if (flag == true) {
                var loading_text = loading_msg != undefined ? loading_msg : "Loading";
                var loader = document.createElement('div');
                loader.className = 'api-loader overlay';
                loader.id = 'apiLoaderDiv';
                loader.align = 'center';
                loader.innerHTML = "<div> " +
                    "<div class='loading-text animate-dot-opacity'> " + loading_text.trim() +
                    "&nbsp;<span></span><span></span><span></span> </div>";
                $compile(loader)($rootScope);
                document.body.appendChild(loader);
            }
            else {
                var ele = document.getElementById('apiLoaderDiv');
                if(ele != undefined) {
                    document.body.removeChild(ele);
                }
            }
    };

    this.showError = function (msg, duration) {
        var time = duration != undefined ? duration : 4000;
        $rootScope.errorMesssage = msg;
        $('#commonError').show();
        $timeout(function () {
            $('#commonError').fadeOut(800)
        }, time);
        // $rootScope.$apply();
    };


    this.drawPieChart = function (id, data, cp, type, title) {
        if ($("#" + id).length == 0) {
            $timeout(function () {
                draw();
            }, 500);
        }
        else {
            draw();
        }
        function draw() {
            c3.generate({
                bindto: '#' + id,
                data: {
                    json: [data],
                    keys: {
                        value: Object.keys(data)
                    },
                    colors: cp,
                    order: null,
                    type: type != undefined ? type : 'pie'
                },
                legend: {
                    position: 'bottom',
                    // show: false
                },
                color: {
                    pattern: cp
                },
                pie: {
                    label: {
                        format: function (value, ratio, id) {
                            return value;
                        }
                    }
                },
                donut: {
                    title: title,
                    width: 40,
                    label: {
                        format: function (value, ratio, id) {
                            return '';
                        }
                    }
                }
            });
        }
    };

    this.drawBarChart = function (id, data, xKey, valueKey, color_fun, bar_width, tooltip, rotateBar) {
        if (tooltip == undefined) {
            tooltip = {};
        }
        if ($("#" + id).length == 0) {
            $timeout(function () {
                draw();
            }, 1000);
        }
        else {
            draw();
        }
        function draw() {
            c3.generate({
                bindto: '#' + id,
                padding: {
                    right: 20,
                    left: 50
                },
                data: {
                    json: data,
                    keys: {
                        x: xKey,
                        value: [valueKey]
                    },
                    order: null,
                    type: 'bar',
                    colors: {
                        value: color_fun
                    },
                    onclick: function (d, element) {
                        console.log(d);
                    }
                },
                bar: {
                    width: bar_width != undefined ? bar_width : 50
                },
                axis: {
                    rotated: rotateBar != undefined ? rotateBar : false,
                    x: {
                        type: 'category',
                        tick: {
                            width: 160
                        }
                    },
                    // y: {
                    //     tick: {
                    //         count: 1
                    //     }
                    // }
                },
                tooltip: {
                    format: {
                        // title: function (d) {
                        //     // return d.toGMTString();
                        //     return new Date(transactionGrossData[d].date).toDateString();
                        // },
                        name: function (name, ratio, id, index) {
                            if (tooltip.name != undefined) {
                                return eval(tooltip.name);
                            }
                            return name;
                        },
                        value: function (value, ratio, id, index) {
                            if (value != 0 && tooltip.value != undefined) {
                                return eval(tooltip.value);
                            }
                            return value;
                        }
                    }
                },
                point: {
                    show: false
                },
                legend: {
                    show: false
                }
            });
        }
    };


    return this;
});