

angular.module("demoApp").filter('replace_char', function() {
    return function(input, find, replace ) {
        if(find == undefined){
            find = '_';
        }
        if(replace == undefined){
            replace = ' ';
        }
        return input.split(find).join(replace);
    }
});
